﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzzService.Interfaces
{
    public interface IFizzBuzzService
    {
        IEnumerable<string> GetFizzBuzzList(int maxLimit);
    }
}
