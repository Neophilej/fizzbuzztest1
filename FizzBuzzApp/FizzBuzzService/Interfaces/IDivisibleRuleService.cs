﻿using FizzBuzzRule.Interface;
using System.Collections.Generic;

namespace FizzBuzzService.Interfaces
{
    public interface IDivisibleRuleService
    {
        IEnumerable<IDivisibleRule> GetMatchedRules(int number);
    }
}
