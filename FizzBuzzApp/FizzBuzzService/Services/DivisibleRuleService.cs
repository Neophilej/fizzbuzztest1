﻿using FizzBuzzRule.Interface;
using FizzBuzzService.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
namespace FizzBuzzService.Services
{
    public class DivisibleRuleService : IDivisibleRuleService
    {
        private readonly IEnumerable<IDivisibleRule> _divisibleRuleList;
        public DivisibleRuleService(IEnumerable<IDivisibleRule> divisibleRuleList)
        {
            _divisibleRuleList = divisibleRuleList;
        }
        public IEnumerable<IDivisibleRule> GetMatchedRules(int number)
        {
            return _divisibleRuleList.Where(rule => rule.Run(number));
        }
    }
}
