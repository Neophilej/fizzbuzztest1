﻿using FizzBuzzRule.Interface;
using FizzBuzzService.Interfaces;
using FizzBuzzService.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzzService.Services
{
    public class FizzBuzzService : IFizzBuzzService
    {
        private readonly IDivisibleRuleService _divisibleRuleServise;
        private readonly IWeekDayCheckRule _weekDayCheckRule;
        public FizzBuzzService(IDivisibleRuleService divisibleRuleServise, IWeekDayCheckRule weekDayCheckRule)
        {
            _divisibleRuleServise = divisibleRuleServise;
            _weekDayCheckRule = weekDayCheckRule;
        }
        public IEnumerable<string> GetFizzBuzzList(int maxLimit)
        {
            var fizzBuzzList = new List<string>();
            for (var itrator = 1; itrator <= maxLimit; itrator++)
            {
                var ruleList = _divisibleRuleServise.GetMatchedRules(itrator);
                if (ruleList.Any())
                {
                    var resultString = string.Join(" ", ruleList.Select(rule => _weekDayCheckRule.ConvertAsPerWeekDayCondition(rule.Result, DateTime.UtcNow.DayOfWeek)));
                    fizzBuzzList.Add(resultString.Trim());
                }
                else
                {
                    fizzBuzzList.Add(itrator.ToString());
                }
            }
            return fizzBuzzList;
        }
    }
}
