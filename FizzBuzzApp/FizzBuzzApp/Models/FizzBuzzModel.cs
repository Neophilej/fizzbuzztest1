﻿using PagedList;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FizzBuzzApp.Models
{
    public class FizzBuzzModel
    {
        [Required(ErrorMessage = "* Input Required")]
        [Display(Name = "Please Enter Number")]
        [Range(1, 1000, ErrorMessage = "* The input value must be between 1 and 1000")]
        public int Number { get; set; }

        public IPagedList<string> FizzBuzzList { get; set; }

        public int? Page { get; set; }
    }
}