﻿using FizzBuzzApp.Controllers;
using FizzBuzzApp.Models;
using FizzBuzzService.Interfaces;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Linq;
namespace FizzBuzzTest.Controllers
{
    [TestFixture]
    public class FizzBuzzControllerTests
    {
        private Mock<IFizzBuzzService> fizzBuzzServiceMock;
        private FizzBuzzController fizzBuzzController;
        private FizzBuzzModel fizzBuzzModel;

        [TestFixtureSetUp]
        public void SetUp()
        {
            this.fizzBuzzModel = new FizzBuzzModel();
            this.fizzBuzzServiceMock = new Mock<IFizzBuzzService>();
            this.fizzBuzzController = new FizzBuzzController(this.fizzBuzzServiceMock.Object);
        }



        [TestCase]
        public void IndexAction_ShouldReturnIndexView()
        {
            //Act
            var resultView = this.fizzBuzzController.Index() as ViewResult;

            //Assert
            Assert.AreEqual("Index", resultView.ViewName);
        }

        [TestCase]
        public void ForInput2GetList_ShouldReturnListOfTwoElemets()
        {
            //Arrange
            this.fizzBuzzModel.Number = 2;
            this.fizzBuzzServiceMock.Setup(fbs => fbs.GetFizzBuzzList(2)).Returns(new List<string>() { "1", "2" });
            var expectedResultList = new List<string>() { "1", "2" };

            //Act
            var actResult = this.fizzBuzzController.GetList(this.fizzBuzzModel) as ViewResult;
            var actualResultModel = actResult.Model as FizzBuzzModel;

            //Assert
            Assert.IsNotNull(actualResultModel);
            CollectionAssert.AreEqual(expectedResultList, actualResultModel.FizzBuzzList);
        }

        [TestCase]
        public void IfNumberIsDivisibleByThree_ShouldReturnFizzAt3IndexOfFizzBuzzList()
        {
            //Arrange
            this.fizzBuzzModel.Number = 3;
            this.fizzBuzzServiceMock.Setup(fbs => fbs.GetFizzBuzzList(3)).Returns(new List<string>() { "1", "2", "fizz" });
            var expectedResult = "fizz";

            var actResult = this.fizzBuzzController.GetList(this.fizzBuzzModel) as ViewResult;
            var actualResultModel = actResult.Model as FizzBuzzModel;

            // Assert
            Assert.IsNotNull(actualResultModel);
            Assert.AreEqual(expectedResult, actualResultModel.FizzBuzzList.ElementAt(2));
        }

        [TestCase]
        public void IfNumberIsDivisibleByFive_ShouldReturnBuzzAt5IndexOfFizzBuzzList()
        {
            //Arrange
            this.fizzBuzzModel.Number = 5;
            this.fizzBuzzServiceMock.Setup(fbs => fbs.GetFizzBuzzList(5)).Returns(new List<string>() { "1", "2", "fizz", "4", "buzz" });
            var expectedResult = "buzz";

            //Act
            var actResult = this.fizzBuzzController.GetList(this.fizzBuzzModel) as ViewResult;
            var actualResultModel = actResult.Model as FizzBuzzModel;

            // Assert
            Assert.IsNotNull(actualResultModel);
            Assert.AreEqual(expectedResult, actualResultModel.FizzBuzzList.ElementAt(4));
        }

        [TestCase]
        public void IfNumberIsDivisibleByFiveAndThree_ShouldReturnFizzAt3_BuzzAt5_FizzBuzzAt15IndexOfFizzBuzzList()
        {
            //Arrange
            this.fizzBuzzModel.Number = 15;
            this.fizzBuzzServiceMock.Setup(fbs => fbs.GetFizzBuzzList(15)).Returns(new List<string>() { "1", "2", "fizz", "4", "buzz", "fizz", "7", "8", 
                "fizz", "buzz", "11", "fizz",
                "13", "14", "fizz buzz"});
            var expectedResultAt3 = "fizz";
            var expectedResultAt5 = "buzz";
            var expectedResultAt15 = "fizz buzz";

            var actResult = this.fizzBuzzController.GetList(this.fizzBuzzModel) as ViewResult;
            var actualResultModel = actResult.Model as FizzBuzzModel;

            // Assert
            Assert.AreEqual(expectedResultAt3, actualResultModel.FizzBuzzList.ElementAt(2));
            Assert.AreEqual(expectedResultAt5, actualResultModel.FizzBuzzList.ElementAt(4));
            Assert.AreEqual(expectedResultAt15, actualResultModel.FizzBuzzList.ElementAt(14));
        }
        [TestCase]
        public void IfNumberIsNotDivisibleByFiveAndThree_ShouldReturnSameNumberAtNthIndexFizzBuzzList()
        {
            //Arrange
            this.fizzBuzzModel.Number = 2;
            this.fizzBuzzServiceMock.Setup(fbs => fbs.GetFizzBuzzList(2)).Returns(new List<string>() { "1", "2" });
            var expectedResult = "2";

            //Act
            var actResult = this.fizzBuzzController.GetList(this.fizzBuzzModel) as ViewResult;
            var actualResultModel = actResult.Model as FizzBuzzModel;


            // Assert
            Assert.IsNotNull(actualResultModel);
            Assert.AreEqual(expectedResult, actualResultModel.FizzBuzzList.ElementAt(1));
        }
        [TestCase]
        public void IfNumberIsGreaterThan20_ShouldReturn20ItemsInFizzBuzzList()
        {
            //Arrange
            this.fizzBuzzModel.Number = 22;
            this.fizzBuzzServiceMock.Setup(fbs => fbs.GetFizzBuzzList(22)).Returns(new List<string>() { "1", "2", "fizz", "4", "buzz", "fizz", "7", "8", 
                "fizz", "buzz", "11", "fizz",
                "13", "14", "fizz buzz","16","17","fizz","19","buzz","fizz","22"});

            var actResult = this.fizzBuzzController.GetList(this.fizzBuzzModel) as ViewResult;
            var actualResultModel = actResult.Model as FizzBuzzModel;

            // Assert
            Assert.IsNotNull(actualResultModel.FizzBuzzList);
            Assert.AreEqual(20, actualResultModel.FizzBuzzList.Count());
        }
    }
}
