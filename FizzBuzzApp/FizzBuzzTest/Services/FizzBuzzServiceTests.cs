﻿using FizzBuzzRule;
using FizzBuzzRule.Interface;
using FizzBuzzService.Interfaces;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzzTest.Services
{
    [TestFixture]
    public class FizzBuzzServiceTests
    {
        private IList<IDivisibleRule> divisibleRuleList;
        private Mock<IDivisibleRule> divisibleRuleMock;
        private Mock<IDivisibleRuleService> divisibleRuleServiceMock;
        private IFizzBuzzService fizzbuzzService;
        private Mock<IWeekDayCheckRule> weekDayCheckRuleMock;
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            this.divisibleRuleMock = new Mock<IDivisibleRule>();
            this.weekDayCheckRuleMock = new Mock<IWeekDayCheckRule>();
            this.divisibleRuleServiceMock = new Mock<IDivisibleRuleService>();

            this.weekDayCheckRuleMock.Setup(wdcr => wdcr.ConvertAsPerWeekDayCondition(It.Is<string>(s => s != "fizz" || s != "buzz"), It.IsAny<DayOfWeek>())).Returns((string x, DayOfWeek y) => x);

            this.fizzbuzzService = new FizzBuzzService.Services.FizzBuzzService(this.divisibleRuleServiceMock.Object, this.weekDayCheckRuleMock.Object);

        }

        [SetUp]
        public void SetUp()
        {
            this.divisibleRuleList = new List<IDivisibleRule>();
        }

        [TestCase]
        public void IfNumberDivisibleByThree_ShouldReturnFizzInResultList()
        {
            //arrange
            this.weekDayCheckRuleMock.Setup(wdcr => wdcr.ConvertAsPerWeekDayCondition("fizz", It.IsAny<DayOfWeek>())).Returns("fizz");
            this.weekDayCheckRuleMock.Setup(wdcr => wdcr.ConvertAsPerWeekDayCondition("buzz", It.IsAny<DayOfWeek>())).Returns("buzz");
            
            this.divisibleRuleMock.Setup(dr => dr.Run(It.Is<int>(number => number % 3 == 0))).Returns(true);
            this.divisibleRuleMock.SetupGet(dr => dr.Result).Returns(Constants.Fizz);
            this.divisibleRuleList.Add(divisibleRuleMock.Object);

            this.divisibleRuleServiceMock.Setup(drs => drs.GetMatchedRules(It.Is<int>(number => number % 3 == 0))).Returns(this.divisibleRuleList);

            // Act
            var result = this.fizzbuzzService.GetFizzBuzzList(3);

            //Assert
            IList<string> expectedresult = new List<string>() { "1", "2", "fizz" };
            CollectionAssert.AreEqual(expectedresult, result);
        }
        [TestCase]
        public void IfNumberDivisibleByFive_ShouldReturnBuzzInResultList()
        {
            //arrange
            this.weekDayCheckRuleMock.Setup(wdcr => wdcr.ConvertAsPerWeekDayCondition("fizz", It.IsAny<DayOfWeek>())).Returns("fizz");
            this.weekDayCheckRuleMock.Setup(wdcr => wdcr.ConvertAsPerWeekDayCondition("buzz", It.IsAny<DayOfWeek>())).Returns("buzz");
            
            this.divisibleRuleMock.Setup(dr => dr.Run(It.Is<int>(number => number % 3 == 0))).Returns(true);
            this.divisibleRuleMock.SetupGet(dr => dr.Result).Returns(Constants.Fizz);
            this.divisibleRuleList.Add(divisibleRuleMock.Object);

            this.divisibleRuleMock = new Mock<IDivisibleRule>();
            this.divisibleRuleMock.Setup(dr => dr.Run(It.Is<int>(number => number % 5 == 0))).Returns(true);
            this.divisibleRuleMock.SetupGet(i => i.Result).Returns(Constants.Buzz);
            var divisibleRuleListForFive = new List<IDivisibleRule>() { divisibleRuleMock.Object };

            this.divisibleRuleServiceMock.Setup(drs => drs.GetMatchedRules(It.Is<int>(number => number % 3 == 0))).Returns(this.divisibleRuleList);
            this.divisibleRuleServiceMock.Setup(drs => drs.GetMatchedRules(It.Is<int>(number => number % 5 == 0))).Returns(divisibleRuleListForFive);

            // Act
            var result = this.fizzbuzzService.GetFizzBuzzList(5);

            //Assert
            IList<string> expectedresult = new List<string>() { "1", "2", "fizz", "4", "buzz" };
            CollectionAssert.AreEqual(expectedresult, result);
        }
        [TestCase]
        public void IfNumberDivisibleByThreeAndFive_ShouldReturnFizzBuzzInResultList()
        {
            //arrange
            this.weekDayCheckRuleMock.Setup(wdcr => wdcr.ConvertAsPerWeekDayCondition("fizz", It.IsAny<DayOfWeek>())).Returns("fizz");
            this.weekDayCheckRuleMock.Setup(wdcr => wdcr.ConvertAsPerWeekDayCondition("buzz", It.IsAny<DayOfWeek>())).Returns("buzz");
            
            this.divisibleRuleMock.Setup(dr => dr.Run(It.Is<int>(number => number % 3 == 0))).Returns(true);
            this.divisibleRuleMock.SetupGet(dr => dr.Result).Returns(Constants.Fizz);
            this.divisibleRuleList.Add(divisibleRuleMock.Object);

            this.divisibleRuleMock = new Mock<IDivisibleRule>();
            this.divisibleRuleMock.Setup(dr => dr.Run(It.Is<int>(number => number % 5 == 0))).Returns(true);
            this.divisibleRuleMock.SetupGet(i => i.Result).Returns(Constants.Buzz);
            var divisibleRuleListForFive = new List<IDivisibleRule>() { divisibleRuleMock.Object };

            this.divisibleRuleServiceMock.Setup(drs => drs.GetMatchedRules(It.Is<int>(number => number % 3 == 0))).Returns(this.divisibleRuleList);
            this.divisibleRuleServiceMock.Setup(drs => drs.GetMatchedRules(It.Is<int>(number => number % 5 == 0))).Returns(divisibleRuleListForFive);
            this.divisibleRuleServiceMock.Setup(d => d.GetMatchedRules(It.Is<int>(t => (t % 3 == 0 && t % 5 == 0)))).Returns(this.divisibleRuleList.Concat(divisibleRuleListForFive).AsEnumerable());

            // Act
            var result = this.fizzbuzzService.GetFizzBuzzList(15);

            //Assert
            IList<string> expectedresult = new List<string>() { "1", "2", "fizz", "4", "buzz", "fizz", "7", "8", 
                "fizz", "buzz", "11", "fizz",
                "13", "14", "fizz buzz"};
            CollectionAssert.AreEqual(expectedresult, result);
        }


        //WednesDayChecks

        [TestCase]
        public void IfNumberDivisibleByThree_WednusDay_ShouldReturnWizzInResultList()
        {
            //arrange
            this.weekDayCheckRuleMock.Setup(wdcr => wdcr.ConvertAsPerWeekDayCondition("fizz", It.IsAny<DayOfWeek>())).Returns("wizz");
            this.weekDayCheckRuleMock.Setup(wdcr => wdcr.ConvertAsPerWeekDayCondition("buzz", It.IsAny<DayOfWeek>())).Returns("wuzz");
            
            this.divisibleRuleMock.Setup(dr => dr.Run(It.Is<int>(number => number % 3 == 0))).Returns(true);
            this.divisibleRuleMock.SetupGet(dr => dr.Result).Returns(Constants.Fizz);
            this.divisibleRuleList.Add(divisibleRuleMock.Object);

            this.divisibleRuleServiceMock.Setup(drs => drs.GetMatchedRules(It.Is<int>(number => number % 3 == 0))).Returns(this.divisibleRuleList);

            // Act
            var result = this.fizzbuzzService.GetFizzBuzzList(3);

            //Assert
            IList<string> expectedresult = new List<string>() { "1", "2", "wizz" };
            CollectionAssert.AreEqual(expectedresult, result);
        }
        [TestCase]
        public void IfNumberDivisibleByFive_WednusDay_ShouldReturnWuzzInResultList()
        {
            //arrange
            this.weekDayCheckRuleMock.Setup(wdcr => wdcr.ConvertAsPerWeekDayCondition("fizz", It.IsAny<DayOfWeek>())).Returns("wizz");
            this.weekDayCheckRuleMock.Setup(wdcr => wdcr.ConvertAsPerWeekDayCondition("buzz", It.IsAny<DayOfWeek>())).Returns("wuzz");
            
            this.divisibleRuleMock.Setup(dr => dr.Run(It.Is<int>(number => number % 3 == 0))).Returns(true);
            this.divisibleRuleMock.SetupGet(dr => dr.Result).Returns(Constants.Fizz);
            this.divisibleRuleList.Add(divisibleRuleMock.Object);

            this.divisibleRuleMock = new Mock<IDivisibleRule>();
            this.divisibleRuleMock.Setup(dr => dr.Run(It.Is<int>(number => number % 5 == 0))).Returns(true);
            this.divisibleRuleMock.SetupGet(i => i.Result).Returns(Constants.Buzz);
            var divisibleRuleListForFive = new List<IDivisibleRule>() { divisibleRuleMock.Object };

            this.divisibleRuleServiceMock.Setup(drs => drs.GetMatchedRules(It.Is<int>(number => number % 3 == 0))).Returns(this.divisibleRuleList);
            this.divisibleRuleServiceMock.Setup(drs => drs.GetMatchedRules(It.Is<int>(number => number % 5 == 0))).Returns(divisibleRuleListForFive);

            // Act
            var result = this.fizzbuzzService.GetFizzBuzzList(5);

            //Assert
            IList<string> expectedresult = new List<string>() { "1", "2", "wizz", "4", "wuzz" };
            CollectionAssert.AreEqual(expectedresult, result);
        }
        [TestCase]
        public void IfNumberDivisibleByThreeAndFive_WednusDay_ShouldReturnWizzWuzzInResultList()
        {
            //arrange
            this.weekDayCheckRuleMock.Setup(wdcr => wdcr.ConvertAsPerWeekDayCondition("fizz", It.IsAny<DayOfWeek>())).Returns("wizz");
            this.weekDayCheckRuleMock.Setup(wdcr => wdcr.ConvertAsPerWeekDayCondition("buzz", It.IsAny<DayOfWeek>())).Returns("wuzz");
            
            this.divisibleRuleMock.Setup(dr => dr.Run(It.Is<int>(number => number % 3 == 0))).Returns(true);
            this.divisibleRuleMock.SetupGet(dr => dr.Result).Returns(Constants.Fizz);
            this.divisibleRuleList.Add(divisibleRuleMock.Object);

            this.divisibleRuleMock = new Mock<IDivisibleRule>();
            this.divisibleRuleMock.Setup(dr => dr.Run(It.Is<int>(number => number % 5 == 0))).Returns(true);
            this.divisibleRuleMock.SetupGet(i => i.Result).Returns(Constants.Buzz);
            var divisibleRuleListForFive = new List<IDivisibleRule>() { divisibleRuleMock.Object };

            this.divisibleRuleServiceMock.Setup(drs => drs.GetMatchedRules(It.Is<int>(number => number % 3 == 0))).Returns(this.divisibleRuleList);
            this.divisibleRuleServiceMock.Setup(drs => drs.GetMatchedRules(It.Is<int>(number => number % 5 == 0))).Returns(divisibleRuleListForFive);
            this.divisibleRuleServiceMock.Setup(d => d.GetMatchedRules(It.Is<int>(t => (t % 3 == 0 && t % 5 == 0)))).Returns(this.divisibleRuleList.Concat(divisibleRuleListForFive).AsEnumerable());

            // Act
            var result = this.fizzbuzzService.GetFizzBuzzList(15);

            //Assert
            IList<string> expectedresult = new List<string>() { "1", "2", "wizz", "4", "wuzz", "wizz", "7", "8", 
                "wizz", "wuzz", "11", "wizz",
                "13", "14", "wizz wuzz"};
            CollectionAssert.AreEqual(expectedresult, result);
        }
    }
}
