﻿using FizzBuzzRule.Interface;
using FizzBuzzService.Interfaces;
using FizzBuzzService.Services;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
namespace FizzBuzzTest.Services
{
    [TestFixture]
    public class DivisibleRuleServiceTests
    {
        private IList<IDivisibleRule> divisibleRuleList;
        private Mock<IDivisibleRule> divisibleRuleMock;
        private IDivisibleRuleService divisibleRuleService;

        [TestFixtureSetUp]
        public void SetUp()
        {
            this.divisibleRuleList = new List<IDivisibleRule>();

            this.divisibleRuleMock = new Mock<IDivisibleRule>();
            this.divisibleRuleMock.Setup(dr => dr.Run(It.Is<int>(number => number % 3 == 0))).Returns(true);
            this.divisibleRuleList.Add(divisibleRuleMock.Object);

            this.divisibleRuleMock = new Mock<IDivisibleRule>();
            this.divisibleRuleMock.Setup(dr => dr.Run(It.Is<int>(number => number % 5 == 0))).Returns(true);
            this.divisibleRuleList.Add(divisibleRuleMock.Object);

            this.divisibleRuleService = new DivisibleRuleService(this.divisibleRuleList);

        }

        [TestCase]
        public void IfNumberDivisibleByThree_ShouldReturnOneRule()
        {
            //act
            var resultRules = this.divisibleRuleService.GetMatchedRules(3);
            //Assert

            Assert.AreEqual(1, resultRules.Count());
        }

        [TestCase]
        public void IfNumberDivisibleByFive_ShouldReturnOneRule()
        {
            //act
            var resultRules = this.divisibleRuleService.GetMatchedRules(5);
            //Assert

            Assert.AreEqual(1, resultRules.Count());
        }

        [TestCase]
        public void IfNumberDivisibleByThreeAndFive_ShouldReturnTwoRule()
        {
            //act
            var resultRules = this.divisibleRuleService.GetMatchedRules(15);
            //Assert

            Assert.AreEqual(2, resultRules.Count());
        }

        [TestCase]
        public void IfNumberDivisibleByThree_ShouldReturnZeroRule()
        {
            //act
            var resultRules = this.divisibleRuleService.GetMatchedRules(7);
            //Assert

            Assert.AreEqual(0, resultRules.Count());
        }

    }
}
