﻿using FizzBuzzRule.Interface;
using FizzBuzzRule.Rules;
using NUnit.Framework;
using System;

namespace FizzBuzzTest.Rules
{
    [TestFixture]
    public class WeekDayCheckRuleTests
    {
        private IWeekDayCheckRule weekDayCheckRule;
        [SetUp]
        public void Setup()
        {
            this.weekDayCheckRule = new WeekDayCheckRule();
        }

        [TestCase]
        public void IfInputFizzAndWednesDay_ShouldReturnWizz()
        {
            //Act
            var result = this.weekDayCheckRule.ConvertAsPerWeekDayCondition("fizz", DayOfWeek.Wednesday);

            //Assert
            Assert.AreEqual("wizz", result);
        }

        [TestCase]
        public void IfInputFizzAndNonWednesDay_ShouldReturnFizz()
        {
            //Act
            var result = this.weekDayCheckRule.ConvertAsPerWeekDayCondition("fizz", DayOfWeek.Monday);

            //Assert
            Assert.AreEqual("fizz", result);
        }

        [TestCase]
        public void IfInputBuzzAndWednesDay_ShouldReturnWuzz()
        {
            //Act
            var result = this.weekDayCheckRule.ConvertAsPerWeekDayCondition("buzz", DayOfWeek.Wednesday);

            //Assert
            Assert.AreEqual("wuzz", result);
        }

        [TestCase]
        public void IfInputBuzzAndNonWednesDay_ShouldReturnBuzz()
        {
            //Act
            var result = this.weekDayCheckRule.ConvertAsPerWeekDayCondition("buzz", DayOfWeek.Monday);

            //Assert
            Assert.AreEqual("buzz", result);
        }

        [TestCase]
        public void IfInputIsOtherThanFizzOrBuzz_AndAnyDay_ShouldReturnSameInput()
        {
            //Act
            var result = this.weekDayCheckRule.ConvertAsPerWeekDayCondition("1", DayOfWeek.Monday);

            //Assert
            Assert.AreEqual("1", result);
        }
    }
}
