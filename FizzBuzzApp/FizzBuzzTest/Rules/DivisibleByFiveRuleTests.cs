﻿using FizzBuzzRule;
using FizzBuzzRule.Interface;
using FizzBuzzRule.Rules;
using NUnit.Framework;

namespace FizzBuzzTest.Rules
{
    [TestFixture]
    public class DivisibleByFiveRuleTests
    {
        private IDivisibleRule _divisiblebyFiveRule;

        [SetUp]
        public void Setup()
        {
            this._divisiblebyFiveRule = new DivisibleByFiveRule();
        }
        [TestCase]
        public void IfNumberDivisibleByFive_ShouldReturnBuzz()
        {
            //act
            var result = this._divisiblebyFiveRule.Run(5);
            var ruleResult = this._divisiblebyFiveRule.Result;

            //Assert
            Assert.True(result);
            Assert.AreEqual(Constants.Buzz, ruleResult);
        }
        [TestCase]
        public void IfNumberNotDivisibleByFive_ShouldReturnEmptyString()
        {
            //act
            var result = this._divisiblebyFiveRule.Run(4);
            var ruleResult = this._divisiblebyFiveRule.Result;

            //Assert
            Assert.False(result);
            Assert.AreEqual(string.Empty, ruleResult);
        }
    }
}
