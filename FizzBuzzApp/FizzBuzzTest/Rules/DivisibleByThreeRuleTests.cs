﻿using FizzBuzzRule;
using FizzBuzzRule.Interface;
using FizzBuzzRule.Rules;
using NUnit.Framework;

namespace FizzBuzzTest.Rules
{
    [TestFixture]
    public class DivisibleByThreeRuleTests
    {
        private IDivisibleRule _divisiblebyThreeRule;

        [SetUp]
        public void Setup()
        {
            this._divisiblebyThreeRule = new DivisibleByThreeRule();
        }
        [TestCase]
        public void IfNumberDivisibleByThree_ShouldReturnFizz()
        {
            //act
            var result = this._divisiblebyThreeRule.Run(3);
            var ruleResult = this._divisiblebyThreeRule.Result;

            //Assert
            Assert.True(result);
            Assert.AreEqual(Constants.Fizz, ruleResult);
        }
        [TestCase]
        public void IfNumberNotDivisibleByThree_ShouldReturnEmptyString()
        {
            //act
            var result = this._divisiblebyThreeRule.Run(4);
            var ruleResult = this._divisiblebyThreeRule.Result;

            //Assert
            Assert.False(result);
            Assert.AreEqual(string.Empty, ruleResult);
        }
    }
}
