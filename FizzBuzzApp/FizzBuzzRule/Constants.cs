﻿
namespace FizzBuzzRule
{
    public static class Constants
    {
        public const string Fizz = "fizz";
        public const string Buzz = "buzz";
        public const string Wizz = "wizz";
        public const string Wuzz = "wuzz";
    }
}
