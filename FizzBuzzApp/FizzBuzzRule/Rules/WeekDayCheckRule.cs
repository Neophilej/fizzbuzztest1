﻿using FizzBuzzRule.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzzRule.Rules
{
    public class WeekDayCheckRule : IWeekDayCheckRule
    {
        public string ConvertAsPerWeekDayCondition(string inputString, DayOfWeek dayOfWeek)
        {
            if (dayOfWeek == DayOfWeek.Wednesday)
            {
                if (inputString == Constants.Fizz)
                    return Constants.Wizz;
                if (inputString == Constants.Buzz)
                    return Constants.Wuzz;
            }
            return inputString;
        }
    }
}
