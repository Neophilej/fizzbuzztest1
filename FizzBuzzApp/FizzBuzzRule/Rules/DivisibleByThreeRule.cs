﻿using FizzBuzzRule.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzzRule.Rules
{
    public class DivisibleByThreeRule : IDivisibleRule
    {
        private string _result=string.Empty;

        public string Result
        {
            get { return _result; }
        }

        public bool Run(int number)
        {
            if (number % 3 == 0)
            {
                _result = Constants.Fizz;
                return true;
            }
            return false;
        }
    }
}
