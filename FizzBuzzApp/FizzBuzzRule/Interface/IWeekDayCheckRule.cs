﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzzRule.Interface
{
    public interface IWeekDayCheckRule
    {
        string ConvertAsPerWeekDayCondition(string inputString, DayOfWeek dayOfWeek);
    }
}
