﻿namespace FizzBuzzRule.Interface
{
    public interface IDivisibleRule
    {
        string Result { get; }
        bool Run(int number);
    }
}
